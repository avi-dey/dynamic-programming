### Longest Common Subsequence (LCS) variants:
[LCS](https://leetcode.com/problems/longest-common-subsequence/)<br>
[Edit Distance](https://leetcode.com/problems/edit-distance/)<br>
https://leetcode.com/problems/distinct-subsequences/<br>
[Min ascii delete sum for two strings](https://leetcode.com/problems/minimum-ascii-delete-sum-for-two-strings/)<br>
My additions: <br>
[1035. Uncrossed Linesncrossed Lines](https://leetcode.com/problems/uncrossed-lines/)<br>


### Palindrome:
https://leetcode.com/problems/palindrome-partitioning-ii/ <br>
[647. Palindromic Substrings](https://leetcode.com/problems/palindromic-substrings/)(dp is not the best here, but good learning curve)<br>

Coin Change variant:
https://leetcode.com/problems/coin-change/
https://leetcode.com/problems/coin-change-2/
https://leetcode.com/problems/combination-sum-iv/
https://leetcode.com/problems/perfect-squares/
https://leetcode.com/problems/minimum-cost-for-tickets/

